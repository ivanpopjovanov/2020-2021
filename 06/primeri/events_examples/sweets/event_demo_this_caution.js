const events = require('events');

class SweetEmmiter extends events.EventEmitter {
  constructor(maxNumberOfSweetsPerDay) {
    super();
    this.maxNumberOfSweetsPerDay = maxNumberOfSweetsPerDay;
    this.numberOfServedSweets = 0;
  }
}

const sweetEmitter = new SweetEmmiter(3);

sweetEmitter.on('enoughSweets', (data) => {
  console.log('Oh, oh, it is too much!');
  sweetEmitter.removeListener('newSweet', serveNewSweet);
});

// works:
const serveNewSweet = function (data) {
  console.log('New sweet is served: ', data);
  this.numberOfServedSweets += 1;
  if (this.numberOfServedSweets == this.maxNumberOfSweetsPerDay) {
    this.emit('enoughSweets');
  }
};

// doesn't work:
// const serveNewSweet = (data) => {
//   console.log('New sweet is served: ', data);
//   this.numberOfServedSweets += 1;
//   if (this.numberOfServedSweets == this.maxNumberOfSweetsPerDay) {
//     this.emit('enoughSweets');
//   }
// };

sweetEmitter.on('newSweet', serveNewSweet);

sweetEmitter.emit('newSweet', 'chocolate');
sweetEmitter.emit('newSweet', 'cake');
sweetEmitter.emit('newSweet', 'ice cream');
sweetEmitter.emit('newSweet', 'pudding');

// console.log(sweetEmitter.numberOfServedSweets);
