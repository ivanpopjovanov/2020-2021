const lodash = require('lodash');

const a = [2, 3, 5, [11, 8], [44]];

const b = lodash.flatten(a);
console.log(b);

const c = lodash.reverse(b);
console.log(c);
