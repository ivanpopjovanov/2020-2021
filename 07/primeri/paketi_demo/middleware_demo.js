const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

const addXToRequest = (req, res, next) => {
  console.log('X');
  req.x = 10;
  next();
};

const addYToRequest = (req, res, next) => {
  console.log('Y');
  req.y = 20;
  next();
};

app.use(addXToRequest);
app.use(addYToRequest);

app.get('/x', (req, res) => {
  const x = req.x || 0;
  const y = req.y || 0;

  res.status(200).json(x + y);
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
