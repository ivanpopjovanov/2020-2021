const express = require('express');
const controller = require('../../controllers/users');
const verifyToken = require('./../authentication');


const router = express.Router();

router.get('/', controller.getAllUsers);
router.get('/:username', controller.getUserByUsername);

router.post('/', verifyToken, controller.addNewUser);
router.put('/', verifyToken, controller.changeUserPassword);
router.delete('/:username', verifyToken, controller.deleteUser);

module.exports = router;
