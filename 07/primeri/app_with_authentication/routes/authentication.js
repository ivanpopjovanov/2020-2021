const jwt = require('jsonwebtoken');

const jwtSecret = process.env.JWT_SECRET || 'pveb_showtime';
const adminUsername = process.env.ADMIN_USERNAME || 'admin';
const adminPassword = process.env.ADMIN_PASSWORD || 'adminpass';
const jwtOpts = { algorithm: 'HS256', expiresIn: '30d' };

const verifyToken = (req, res, next) => {
  var token = req.headers['x-access-token'];

  if (!token) {
    return res.status(403).json({ auth: false, message: 'No token provided.' });
  }

  jwt.verify(token, jwtSecret, (error, adminData) => {
    if (error) {
      return res
        .status(500)
        .json({ auth: false, message: 'Failed to authenticate token.' });
    }
    if (
      adminData.adminUsername != adminUsername ||
      adminData.adminPassword != adminPassword
    ) {
      return res.status(403).json({auth: false, message: 'Wrong username and/or password!'});
    }
    
    next();
  });
};

module.exports = verifyToken;
