import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { ProductPopularity } from '../models/product-popularity';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  ProductPopularityEnum = ProductPopularity;

  @Input()
  product: Product;

  constructor() {}

  ngOnInit() {
  }
}
