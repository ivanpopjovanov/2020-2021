import { User } from "src/app/users/models/user";
import { ProductState } from "../../models/product-state";

export interface GetProductResponseBody {
  _id: string;
  name: string;
  price: number;
  description: string;
  forSale: boolean;
  state: ProductState;
  owner: User;
  imgUrls: string[];
  stars: number;
}

export interface GetProductsResponseBody {
  docs: GetProductResponseBody[];
  hasNextPage: boolean;
  hasPrevPage: boolean;
  limit: number;
  nextPage?: number;
  page: number;
  pagingCounter: number;
  prevPage?: number;
  totalDocs: number;
  totalPages: number;
}
