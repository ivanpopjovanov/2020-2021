const User = require('./userModel');

module.exports.registerUser = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  const email = req.body.email;
  const name = req.body.name;

  try {
    if (!username || !password || !email || !name) {
      const error = new Error('Please provide all data for a new user');
      error.status = 400;
      throw error;
    }

    const jwt = await User.registerNewUser(username, password, email, name);
    return res.status(201).json({
      token: jwt,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.loginUser = async (req, res, next) => {
  const username = req.username;

  try {
    const jwt = await User.getUserJWTByUsername(username);
    return res.status(201).json({
      token: jwt,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.changeUserInfoData = async (req, res, next) => {
  const username = req.username;
  const name = req.body.name;
  const email = req.body.email;

  try {
    if (!email || !name) {
      const error = new Error('New data cannot be empty');
      error.status = 400;
      throw error;
    }

    const jwt = await User.updateUserData(username, name, email);
    return res.status(201).json({
      token: jwt,
    });
  } catch (err) {
    next(err);
  }
};
