const express = require('express');

const router = express.Router();

const controller = require('./userController');
const authentication = require('../utils/authentication');

router.post('/register', controller.registerUser);
router.post('/login', authentication.canAuthenticate, controller.loginUser);
router.patch('/', authentication.isAuthenticated, controller.changeUserInfoData);

module.exports = router;
