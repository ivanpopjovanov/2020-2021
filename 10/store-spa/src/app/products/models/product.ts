export enum ProductPopularity {
  VeryPopular = 'Very Popular',
  Popular = 'Popular',
  Average = 'Average',
  NotPopular = 'Not popular',
}

export class Product {
  constructor(
    public name: string,
    public description: string,
    public price: number,
    public forSale: boolean,
    public imgUrl: string,
    public owner: string,
    public stars: number = 0
  ) {}

  get popularity(): ProductPopularity {
    if (this.stars >= 200) {
      return ProductPopularity.VeryPopular;
    }
    if (this.stars >= 100) {
      return ProductPopularity.Popular;
    }
    if (this.stars >= 50) {
      return ProductPopularity.Average;
    }
    return ProductPopularity.NotPopular;
  }
}
