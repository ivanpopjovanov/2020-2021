import { Component, Input, OnInit } from '@angular/core';
import { Product, ProductPopularity } from '../models/product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  PopularityEnum = ProductPopularity;

  @Input()
  products: Product[] = [];

  constructor() {}

  ngOnInit() {}
}
