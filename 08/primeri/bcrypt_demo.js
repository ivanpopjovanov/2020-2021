const bcrypt = require('bcrypt');
const SALT_ROUNDS = 10;

const bcryptDemo = async () => {
  const user = {
    username: 'test',
    email: 'test@gmail.com',
    password: 'JSIsCool',
  };

  // funkciju hash koristimo za generisanje kriptovane sifre
  // prilikom ovog generisanja koriste se automatski generisani zacin (engl. salt) 
  const newPassword = await bcrypt.hash(user.password, SALT_ROUNDS);
  console.log(newPassword);

  // za proveru poklapanja sifri koristimo funkciju compare
  let isCorrectPassword;
  isCorrectPassword = await bcrypt.compare('RandomPassword', newPassword);
  console.log(isCorrectPassword);

  isCorrectPassword = await bcrypt.compare('JSIsCool', newPassword);
  console.log(isCorrectPassword);


  // ukoliko je potrebno da generisemo svoj "zacin" za skrivanje sifre
  // mozemo iskoristiti genSalt funkciju 
  const salt = await bcrypt.genSalt(SALT_ROUNDS);
  const newSaltedPassword = await bcrypt.hash(user.password, salt);
  console.log(newSaltedPassword);
};

bcryptDemo();